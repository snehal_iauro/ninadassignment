import React, { Component } from "react";
import "./home.scss";

import Link from "../../components/Link/Link";

class Home extends Component {
  state = {};

  render() {
    return (
      <>
        <div className="home-screen">
          <div className="home-screen-title">Iauro</div>
          <Link to={"/dashboard"}>
            <div className="home-screen-tab">GlobalLink Jobs Dashboard</div>
          </Link>
        </div>
      </>
    );
  }
}

export default Home;
