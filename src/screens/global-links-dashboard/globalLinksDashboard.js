import React, { Component } from "react";
import "./globalLinksDashboard.scss";

import GoBack from "../../components/GoBack/GoBack";
import Link from "../../components/Link/Link";

import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";

import { formatLongDate } from "../../services/util.services";
import Data from "../../services/data.json";

class GlobalLinksDashboard extends Component {
  constructor(props) {
    super(props);
    let joblist = [];
    if (Data.status === 200) {
      joblist = Data.response_data.jobs_list;
    }

    this.state = {
      joblist,
      page: 0,
      rowsPerPage: 10,
    };
  }

  handleChangePage = (e, newPage) => {
    this.setState({ page: newPage });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: +event.target.value });
    this.handleChangePage(event, 0);
  };

  render() {
    const columns = [
      {
        id: "pd_submission_id",
        label: "ID",
        minWidth: 70,
        getValue: (row) => {
          return row.pd_submission_id.map((data, i) =>
            i === row.pd_submission_id.length - 1 ? (
              <Link
                to={{
                  pathname: "/job-details",
                  jobDetails: row,
                }}
                style={{ textDecoration: "underline" }}
              >
                {data}
              </Link>
            ) : (
              <>
                <Link
                  to={{
                    pathname: "/job-details",
                    jobDetails: row,
                  }}
                  style={{ textDecoration: "underline" }}
                >
                  {data + ","}
                </Link>{" "}
              </>
            )
          );
        },
      },
      {
        id: "submission_name",
        label: "Submission Name",
        minWidth: 120,
        getValue: (row) => {
          return (
            <Link
              to={{
                pathname: "/job-details",
                jobDetails: row,
              }}
              style={{ textDecoration: "underline" }}
            >
              {row["submission_name"]}
            </Link>
          );
        },
      },
      {
        id: "submitter",
        label: "Created By",
        minWidth: 120,
        getValue: (row, column) => row["submitter"],
      },
      {
        id: "state_name",
        label: "Translation Status",
        minWidth: 120,
        getValue: (row, column) => row.state.state_name,
      },
      {
        id: "source_locale",
        label: "Source Locale",
        minWidth: 120,
        getValue: (row, column) => row.source_locale.locale_display_name,
      },
      {
        id: "target_locale",
        label: "Target Locale",
        minWidth: 120,
        getValue: (row, column) => row.target_locale.locale_display_name,
      },
      {
        id: "created_at",
        label: "Job Created On",
        minWidth: 120,
        getValue: (row, column) => formatLongDate(row["created_at"]),
      },
      {
        id: "due_date",
        label: "Job Due Date",
        minWidth: 120,
        getValue: (row, column) => formatLongDate(row["due_date"]),
      },
      {
        id: "state",
        label: "Job Delivered On",
        minWidth: 120,
        getValue: (row, column) => formatLongDate(row.state.last_update_date),
      },
    ];

    return (
      <>
        <div className="global-link-dashboard">
          <GoBack to={"/"} content={"GlobalLink Jobs Dashboard"} />
          <div className="dashboard-data-table">
            <Paper style={{ width: "100%" }}>
              <TableContainer style={{ maxHeight: "440px" }}>
                <Table aria-label="table">
                  <TableHead>
                    <TableRow>
                      {columns.map((column, i) => (
                        <TableCell
                          key={i}
                          align={column.align}
                          style={{ minWidth: column.minWidth }}
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.joblist
                      .slice(
                        this.state.page * this.state.rowsPerPage,
                        this.state.page * this.state.rowsPerPage +
                          this.state.rowsPerPage
                      )
                      .map((row, i) => {
                        return (
                          <TableRow hover tabIndex={-1} key={i}>
                            {columns.map((column, j) => {
                              return (
                                <TableCell key={j} align={column.align}>
                                  {column.getValue ? column.getValue(row) : ""}
                                </TableCell>
                              );
                            })}
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={this.state.joblist.length}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </>
    );
  }
}

export default GlobalLinksDashboard;
