import React, { Component } from "react";
import "./globalLinksJobDetails.scss";

import GoBack from "../../components/GoBack/GoBack";

import { formatLongDate } from "../../services/util.services";

class GlobalLinksJobDetails extends Component {
  constructor(props) {
    super(props);
    let jobDetails = {};
    if (this.props.location.jobDetails) {
      jobDetails = this.props.location.jobDetails;
    } else {
      this.props.history.push("/dashboard");
    }
    console.log("jobDetails", jobDetails);

    this.state = { jobDetails };
  }

  render() {
    return (
      <>
        <div className="global-link-job-details">
          <GoBack to={"/dashboard"} content={"GlobalLink Job Details"} />
          <div className="job-details-section-title">Details</div>
          <div className="job-details-row">
            <div className="job-details-key">ID</div>
            <div className="job-details-value">
              :{" "}
              {this.state.jobDetails.pd_submission_id &&
                this.state.jobDetails.pd_submission_id.map((data, i) =>
                  i === this.state.jobDetails.pd_submission_id.length - 1
                    ? data
                    : data + ", "
                )}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Submission Name</div>
            <div className="job-details-value">
              {this.state.jobDetails.submission_name &&
                ": " + this.state.jobDetails.submission_name}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Source Locale</div>
            <div className="job-details-value">
              {this.state.jobDetails.source_locale &&
                ": " + this.state.jobDetails.source_locale.locale_display_name}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Target Locale</div>
            <div className="job-details-value">
              {this.state.jobDetails.target_locale &&
                ": " + this.state.jobDetails.target_locale.locale_display_name}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Status</div>
            <div className="job-details-value">
              {this.state.jobDetails.state &&
                ": " + this.state.jobDetails.state.state_name}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Created Date</div>
            <div className="job-details-value">
              {": " + formatLongDate(this.state.jobDetails.created_at)}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Due Date</div>
            <div className="job-details-value">
              {": " + formatLongDate(this.state.jobDetails.due_date)}
            </div>
          </div>
          <div className="job-details-row">
            <div className="job-details-key">Created By</div>
            <div className="job-details-value">
              {": " + this.state.jobDetails.submitter}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default GlobalLinksJobDetails;
