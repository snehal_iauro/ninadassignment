import React, { Component } from "react";
import "./Link.scss";

import { Link } from "react-router-dom";

class RouteLink extends Component {
  render() {
    return (
      <>
        <Link
          to={this.props.to}
          className={
            "component-link " +
            (this.props.className ? this.props.className : "")
          }
          style={this.props.style}
          params={this.props.params}
        >
          {this.props.children}
        </Link>
      </>
    );
  }
}

export default RouteLink;
