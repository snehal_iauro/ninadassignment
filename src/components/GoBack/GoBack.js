import React, { Component } from "react";
import "./GoBack.scss";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import Link from "../../components/Link/Link";

class GoBack extends Component {
  state = {};

  render() {
    return (
      <>
        <div className="component-go-back">
          <Link to={this.props.to}>
            <ArrowBackIcon className="component-go-back-icon" />
          </Link>
          <div className="component-go-back-content">{this.props.content}</div>
        </div>
      </>
    );
  }
}

export default GoBack;
