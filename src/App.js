import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./styles/main.scss";

import Home from "./screens/home/home";
import GlobalLinksDashboard from "./screens/global-links-dashboard/globalLinksDashboard";
import GlobalLinksJobDetails from "./screens/global-links-job-details/globalLinksJobDetails";

function App() {
  return (
    <Router>
      <div className="app-container">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/dashboard" component={GlobalLinksDashboard} />
          <Route path="/job-details" component={GlobalLinksJobDetails} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
