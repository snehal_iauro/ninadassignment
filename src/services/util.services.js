export const monthNames = [
  { long: "January", short: "Jan" },
  { long: "February", short: "Feb" },
  { long: "March", short: "Mar" },
  { long: "April", short: "April" },
  { long: "May", short: "May" },
  { long: "June", short: "Jun" },
  { long: "July", short: "Jul" },
  { long: "August", short: "Aug" },
  { long: "September", short: "Sep" },
  { long: "October", short: "Oct" },
  { long: "November", short: "Nov" },
  { long: "December", short: "Dec" },
];

export function formatLongDate(date = new Date()) {
  let actualDate = new Date(date);
  let dd = actualDate.getDate();
  if (dd < 10) {
    dd = "0" + dd;
  }
  let mm = monthNames[actualDate.getMonth()].short;
  let yy = actualDate.getFullYear();
  let hrs = actualDate.getHours();
  let min = actualDate.getMinutes();
  return dd + " " + mm + " " + yy + " | " + hrs + ":" + min;
}
